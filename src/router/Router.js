import React, { useState } from "react";
import PublicRouter from "./PublicRouter";
import AuthenticatedRouter from "./AuthenticatedRouter";

function Router() {
  const [user, setUser] = useState();

  // mock user if a localStorage present
  const token = localStorage.getItem(`${process.env.REACT_APP_NAME}_token`);
  if (token && !user) {
    setUser({
      firstName: "Nikhil",
      lastName: "Nath R",
      email: "nikhilnath1000@gmail.com",
    });
  }

  return user ? <AuthenticatedRouter /> : <PublicRouter />;
}

export default Router;
