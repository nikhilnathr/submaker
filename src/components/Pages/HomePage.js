import React from "react";
import useHeading from "./useHeading";

function HomePage() {
  useHeading("Home");

  return <div>HomePage</div>;
}

export default HomePage;
