import React from "react";
import useHeading from "./useHeading";

function NotFoundPage() {
  useHeading("Oops! Not Found");
  return <div>NotFoundPage</div>;
}

export default NotFoundPage;
