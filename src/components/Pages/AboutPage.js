import React from "react";
import useHeading from "./useHeading";

function AboutPage() {
  useHeading("About");

  return <div>AboutPage</div>;
}

export default AboutPage;
