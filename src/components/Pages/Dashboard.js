import React from "react";
import useHeading from "./useHeading";

function DashboardPage() {
  useHeading("Dashboard");
  return <div>DashboardPage</div>;
}

export default DashboardPage;
