import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { Typography } from "@material-ui/core";

function Terms() {
  const [open, setOpen] = useState(false);

  function handleClose() {
    setOpen(false);
  }

  function onOpen() {
    setOpen(true);
  }

  return [
    <Dialog
      onClose={handleClose}
      aria-labelledby="Terms and conditions"
      open={open}
    >
      <DialogTitle id="customized-dialog-title" onClose={handleClose}>
        Terms and Conditions
      </DialogTitle>
      <DialogContent dividers>
        <Typography gutterBottom>
          By registering a new account at {process.env.REACT_APP_NAME} you are
          accepting the following conditions,
        </Typography>
        <Typography gutterBottom>
          I will uphold my integrity to the Floor gang and I will always respect
          the Floor. The ceiling will remain my enemy throughout my life.
        </Typography>
        <Typography gutterBottom>
          If found guilty of partnering on being in contact (in a friendly
          manner) with the ceiling I accept any kind of punishment given by the
          great mighty Lord.
        </Typography>
      </DialogContent>
      <DialogActions>
        <Button autoFocus onClick={handleClose} color="primary">
          Close
        </Button>
      </DialogActions>
    </Dialog>,
    onOpen,
  ];
}

export default Terms;
