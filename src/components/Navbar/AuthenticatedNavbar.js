import React from "react";
import PropTypes from "prop-types";
import DashboardIcon from "@material-ui/icons/Dashboard";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import VpnKeyIcon from "@material-ui/icons/VpnKey";
import Navbar from "./Navbar";
import { navigate } from "hookrouter";

function AuthenticatedNavbar({ page }) {
  const drawer = [
    {
      path: "/",
      text: "Dashboard",
      icon: <DashboardIcon />,
    },
    {
      path: "/user/nikhilnathr",
      text: "Profile",
      icon: <AccountCircleIcon />,
    },
    { divider: true },
    {
      fn: async () => {
        localStorage.removeItem(`${process.env.REACT_APP_NAME}_token`);
        navigate("/");
        window.location.reload();
      },
      text: "Logout",
      icon: <VpnKeyIcon />,
    },
  ];

  return <Navbar drawer={drawer} page={page} />;
}

AuthenticatedNavbar.propTypes = {
  page: PropTypes.element,
};

export default AuthenticatedNavbar;
