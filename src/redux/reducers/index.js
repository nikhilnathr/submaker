import { combineReducers } from "redux";
import appState from "./appState";
import apiState from "./apiState";

const allReducers = combineReducers({
  appState: appState,
  api: apiState,
});

export default allReducers;
