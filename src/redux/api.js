export default {
  register: {
    path: "/api/register",
    method: "POST",
    noAuth: true,
  },
};
